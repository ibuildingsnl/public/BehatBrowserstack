BehatBrowserstack
=================

Browserstack automate & local extension for behat.
 
[https://www.browserstack.com/automate]()

The example configuration is inside [behat.yml](./behat.yml) and the required dependencies can be found in [composer.json](./composer.json)

Run from client (VM) or host machine
------------------------------------ 

USERNAME and ACCESS_KEY can be found at [https://www.browserstack.com/accounts/settings]()

1. Install composer dependencies.

    ``` Composer.json
      "repositories": [
            {
                "type": "git",
                "url": "https://gitlab.com/ibuildingsnl/public/BehatBrowserstack.git"
            },
      ]        
    ```
    
    Require extension
    ```
    composer require Ibuildings/BehatBrowserstack
    ```

2. Run local server:
 
    ```
    cd web
    php -S localhost:8000
    ```
 
3. Download local for your platform bin folder

    [https://www.browserstack.com/local-testing](https://www.browserstack.com/local-testing)

4. Export browserstack env variables    
   
   ```
    export BROWSERSTACK_USERNAME=<USERNAME>
    export BROWSERSTACK_ACCESS_KEY=<ACCESS_KEY>
   ```

5. Run behat with profile

   ```
    ./vendor/bin/behat -p bs_osx_chrome
   ```

Chrome local (on your host machine)
-----------------------------------

1. Install driver 
    
    ```
    yarn install
    ```

2. Run driver with:

    ```
    yarn chromedriver
    ```

    Keep in mind when you run behat from virtual machine you need to specify a IP whitelist:
    
    ```
    yarn chromedriver --whitelisted-ips=192.168.12.34,10.0.2.15
    ```
       
    And change  "wd_host: 'http://localhost:9515'" in behat.yml


3. Start test (from Host or VM)

   ```
    ./vendor/bin/behat -p chrome
   ```

Reads 
-----

https://github.com/docksal/behat
