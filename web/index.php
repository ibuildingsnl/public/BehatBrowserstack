<html xmlns="http://www.w3.org/1999/html">
<title>Behat example</title>
</html>


<body>

<label for="textfield" >Text:</label>
<input id="textfield" type="text" value=""/>
<input id="button" type="button" value="Add"/>

</body>

<script>

    function onclick () {
        var element = document.createElement('div');
        element.innerText = 'Hallo ' + window.textfield.value;
        document.body.appendChild(element);
        window.textfield.value = '';
    }

    window.button.onclick = onclick;

</script>
