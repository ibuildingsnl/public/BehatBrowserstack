<?php

namespace Example\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\MinkExtension\Context\MinkContext;

class BrowserStackContext implements Context
{

    /**
     * MinkContext.
     *
     * @var \Behat\MinkExtension\Context\MinkContext
     */
    protected $minkContext;

    /**
     * Fetch the required contexts.
     *
     * @param \Behat\Behat\Hook\Scope\BeforeScenarioScope $scope
     *
     * @BeforeScenario
     */
    public function gatherContexts(BeforeScenarioScope $scope)
    {
        $environment = $scope->getEnvironment();
        $this->minkContext = $environment->getContext(MinkContext::class);
    }
}
