@javascript @fails
Feature: Should mark this feature as failed

  Scenario: Can add a strawberry

    Given I am on "/"

    When I fill in "textfield" with "apple"
    And I press "Add"

    Then I should see text matching "peer"
