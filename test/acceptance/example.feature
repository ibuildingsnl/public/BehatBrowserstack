@javascript
Feature: Can add text to the page with Javascript

  Scenario: Can add multiple lines

    Given I am on "/"

    When I fill in "textfield" with "appel"
    And I press "Add"

    When I fill in "textfield" with "peer"
    And I press "Add"

    Then I should see text matching "appel"
    Then I should see text matching "peer"
