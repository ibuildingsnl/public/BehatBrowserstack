@javascript
Feature: Can add fruit to the page with Javascript

  Scenario: Can add a strawberry

    Given I am on "/"

    When I fill in "textfield" with "strawberry"
    And I press "Add"

    Then I should see text matching "strawberry"

  Scenario: Can add a apple

    Given I am on "/"

    When I fill in "textfield" with "apple"
    And I press "Add"

    Then I should see text matching "apple"
