<?php

namespace Ibuildings\BehatBrowserstack;

use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Exception\DriverException;

class BrowserstackSelenium2Driver extends Selenium2Driver {

  /**
   * @var BrowserstackLocalProcessRunner
   */
  private $browserstackLocal;

  private $enableBrowserstackTunnel;

  private $localIdentifier;

  private $client;

  public function __construct(
    BrowserstackClient $client,
    string $browserName = 'firefox',
    ?array $desiredCapabilities = NULL,
    string $wdHost = 'http://localhost:4444/wd/hub',
    bool $debug = false
  ) {
    $localIdentifier = uniqid('bs', FALSE);
    $this->localIdentifier = $localIdentifier;
    $this->enableBrowserstackTunnel = $desiredCapabilities['browserstack-tunnel'] ?? FALSE;
    $desiredCapabilities['browserstack.localIdentifier'] = $localIdentifier;
    $this->browserstackLocal = new BrowserstackLocalProcessRunner($localIdentifier, $debug);
    parent::__construct($browserName, $desiredCapabilities, $wdHost);
    $this->client = $client;
  }

  /**
   * The endless loop is only used when there are no available slot in the pending queue
   * of browserstack.
   *
   * TODO: clean up this code.
   *
   * @throws \Behat\Mink\Exception\DriverException
   */
  public function start() {
    if (!$this->enableBrowserstackTunnel) {
      parent::start();
      return;
    }
    $attempt = 1;
    while (TRUE) {
      try {
        $this->browserstackLocal->start();
        parent::start();
        print sprintf(
          'Selenium2 connected with browserstack local! (localIdentifier: %s)' . PHP_EOL,
          $this->localIdentifier
        );
        // We have a connection!, can stop the endless loop.
        return;
      }
      catch (DriverException $e) {
        print sprintf('Selenium2 connection attempt "%d" failed with error: "%s"' . PHP_EOL,
          $attempt, $e->getMessage());
        flush();

        // Every 5 times we retry to restart Browserstack local.
        if ($attempt % 5 === 0) {
          $this->browserstackLocal->stop();
        }
        else {
          sleep(4);
        }
      }
      $attempt++;
    }
  }

  public function markFailed(string $reason) {
    $this->client->markFailed($this->getWebDriverSessionId(), $reason);
  }

  public function updateTestName(string $name) {
    $session = $this->client->getSession($this->getWebDriverSessionId());
    if ($session->name === $name) {
      return;
    }
    // We only need a new sessions when it's not the initial one.
    if ($session->name !== 'pending') {
      parent::stop();
      parent::start();
    }
    $this->client->updateName($this->getWebDriverSessionId(), $name);
    $this->printBrowserstackUrl();
  }

  public function stop() {
    $this->browserstackLocal->stop();
    parent::stop();
  }

  public function printBrowserstackUrl() {
    try {
      $session = $this->client->getSession($this->getWebDriverSessionId());
      print 'Public Browserstack url:' . PHP_EOL;
      print $session->public_url . PHP_EOL;
    }
    catch (\Exception $exception) {
      print sprintf('Could not fetch Browserstack url, because: %s' . PHP_EOL, $exception->getMessage());
    }
    // Otherwise behat quits before printing to terminal.
    flush();
  }

}
