<?php

namespace Ibuildings\BehatBrowserstack;

use Symfony\Component\Process\Process;

class BrowserstackLocalProcessRunner {

  /**
   * @var \Symfony\Component\Process\Process
   */
  private $process;

  /**
   * To support Multiple Local Testing Connections.
   *
   * If you are using same account to test multiple applications,
   * you can setup named connection using the localIdentifier option.
   *
   * @var string
   */
  private $localIdentifier;

  private $running = FALSE;

  private $debug;

  public function __construct(string $localIdentifier, bool $debug) {
    $this->localIdentifier = $localIdentifier;
    $this->debug = $debug;
  }

  public function start() {
    if ($this->process && !$this->process->hasBeenStopped()) {
      return;
    }
    $command = sprintf(
      'BrowserStackLocal --force  --local-identifier="%s" --key="%s" &',
      $this->localIdentifier,
      EnvironmentVariables::password()
    );
    $this->process = new Process($command, NULL, getenv(), NULL, NULL);
    $this->process->start(function () {
      $output = $this->printAndGetIncrementalOutput();
      if (strpos($output,
          'You can now access your local server(s) in our remote browser.') !== FALSE) {
        $this->running = TRUE;
      }
    });

    // Minor attempt to wait until we have an actual
    // running Browserstack local process.
    // The Process class, does not always returns the stdout async..
    // Depends on the current env.
    $currentTime = time();
    while ((time() - $currentTime) < 5 && !$this->running) {
      sleep(1);
    }
  }

  public function stop(): void {
    if ($this->process) {
      /**
       * TODO: the process should be async, but it its not.. so we use a & to start it in the background.
       */
      $process = new Process('killall BrowserStackLocal');
      $process->run();
      $this->process->stop();
      $this->process = NULL;
      $this->running = FALSE;
    }
  }

  private function printAndGetIncrementalOutput() {
    $errorOutput = $this->process->getIncrementalErrorOutput();
    if ($errorOutput) {
      print $errorOutput . PHP_EOL;
    }
    $output = $this->process->getIncrementalOutput();
    if ($output && $this->debug) {
      print $output . PHP_EOL;
    }
    flush();
    return $output;
  }

}
