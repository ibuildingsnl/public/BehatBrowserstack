<?php

namespace Ibuildings\BehatBrowserstack;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\Environment\ContextEnvironment;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Testwork\Tester\Result\TestResult;

class BrowserstackContext implements Context {

  /**
   * @var \Behat\MinkExtension\Context\MinkContext
   */
  private $minkContext;

  /**
   * @var \Behat\Behat\Hook\Scope\BeforeScenarioScope
   */
  private $scope;

  /**
   * Fetch the required contexts.
   *
   * @BeforeScenario
   */
  public function gatherContexts(BeforeScenarioScope $scope): void {
    $this->scope = $scope;
    $this->minkContext = self::getContextFromEnv($scope->getEnvironment());
    $this->setBrowserstackTestName();
  }

  /**
   * Fetch the required contexts.
   */
  public function setBrowserstackTestName(): void {
    $driver = self::getContextFromEnv($this->scope->getEnvironment())->getMink()->getSession()->getDriver();
    if (!$driver instanceof BrowserstackSelenium2Driver) {
      return;
    }
    $featureTitle = $this->scope->getFeature()->getTitle();
    $scenarioTitle = $this->scope->getScenario()->getTitle();
    $driver->updateTestName(sprintf('[%s] %s', $featureTitle, $scenarioTitle));
  }

  /**
   * @AfterStep
   */
  public function stopAfterFail(AfterStepScope $scope): void {
    if ($scope->getTestResult()->getResultCode() === TestResult::FAILED) {
      $this->markBrowserstackAsFailed();
    }
  }

  public function markBrowserstackAsFailed() {
    $driver = $this->minkContext->getMink()->getSession()->getDriver();
    if (!$driver instanceof BrowserstackSelenium2Driver) {
      return;
    }
    $feature = $this->scope->getFeature();
    $scenario = $this->scope->getScenario();
    $driver->markFailed(sprintf(
      '%s - %s',
      $feature->getDescription(),
      $scenario->getTitle()
    ));
  }

  protected static function getContextFromEnv(ContextEnvironment $environment) {
    $contexts = $environment->getContexts();
    foreach ($contexts as $context) {
      if ($context instanceof MinkContext) {
        return $context;
      }
    }
    throw new \Exception('There should be a mink context');
  }

}
