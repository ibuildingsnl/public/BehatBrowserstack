<?php

namespace Ibuildings\BehatBrowserstack;

use Behat\Mink\Driver\Selenium2Driver;
use Webmozart\Assert\Assert;

class EnvironmentVariables extends Selenium2Driver {
  private static $now;

  public static function username(): string {
    $username = getenv('BROWSERSTACK_USERNAME');
    Assert::stringNotEmpty(
      $username,
      'BROWSERSTACK_USERNAME env variable should be defined'
    );
    return $username;
  }

  public static function password(): string {
    $password = getenv('BROWSERSTACK_ACCESS_KEY');
    Assert::stringNotEmpty(
      $password,
      'BROWSERSTACK_ACCESS_KEY env variable should be defined'
    );
    return $password;
  }

  public static function build(): string {
    $build = getenv('BROWSERSTACK_BUILD');
    if ($build) {
      return $build;
    }
    if (getenv('GITLAB_CI')) {
      return self::buildForGitlabCI();
    }
    if (getenv('TRAVIS')) {
      return self::buildForTravisCI();
    }
    if (!self::$now) {
      self::$now = (new \DateTime())->format('Y-m-d H:i:s');
    }
    return sprintf('local "%s" %s', gethostname(), self::$now);
  }

  private static function buildForGitlabCI(): string {
    return sprintf(
      '%s - %s',
      getenv('CI_PIPELINE_ID'),
      getenv('CI_COMMIT_REF_NAME')
    );
  }

  private static function buildForTravisCI(): string {
    return sprintf(
      '%s - %s',
      getenv('TRAVIS_BUILD_ID'),
      getenv('TRAVIS_COMMIT')
    );
  }

}
