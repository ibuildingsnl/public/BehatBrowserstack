<?php

namespace Ibuildings\BehatBrowserstack\Command;

use Webmozart\Assert\Assert;

class ScenarioStatsCollection {

  /**
   * @var ScenarioStats[]
   */
  private $stats = [];

  /**
   * @param \Ibuildings\BehatBrowserstack\Command\ScenarioStats[] $stats
   */
  public function __construct(array $stats = []) {
    $this->stats = $stats;
    Assert::allIsInstanceOf($stats, ScenarioStats::class);
  }

  public function get(string $featureName, string $scenarioName): ScenarioStats {
    $stat = new ScenarioStats($featureName, $scenarioName);
    $label = $stat->getLabel();
    if (empty($this->stats[$label])){
      $this->stats[$label] = $stat;
    }
    return $this->stats[$label];
  }

  /**
   * @return ScenarioStats[]
   */
  public function getStats(): array {
    // Create shallow copy.
    return array_merge([], $this->stats);
  }

  public function orderByFailed(): ScenarioStatsCollection {
    $stats = $this->getStats();
    usort($stats, function (ScenarioStats $a, ScenarioStats $b) {
      if ($a->getFails() === $b->getFails()) {
        return 0;
      }
      return $a->getFails() > $b->getFails() ? 1 : -1;
    });
    return new ScenarioStatsCollection($stats);
  }

  /**
   * @param ScenarioStats[] $scenarioStats
   *
   * @return \Ibuildings\BehatBrowserstack\Command\ScenarioStatsCollection
   */
  public function mergeSame(array $scenarioStats): self {
    foreach ($scenarioStats as $stat) {
      if (!empty($this->stats[$stat->getLabel()])) {
        $this->stats[$stat->getLabel()]->addStats($stat);
      }
    }
    return $this;
  }

  public function toString(): string {
    $percentage = $this->count() === 0 ? 0 : ((float) $this->getFails()) / ((float) $this->count()) * 100.0;
    $fail = sprintf('%d/%d', $this->getFails(), $this->count());
    return sprintf('%8s | (%3d%%)', $fail, $percentage);
  }

  public function getFails(): int {
    return array_reduce($this->stats, function ($fails, ScenarioStats $stats) {
      return $stats->getFails() + $fails;
    }, 0);
  }

  public function count(): int {
    return array_reduce($this->stats, function ($counts, ScenarioStats $stats) {
      return $stats->count() + $counts;
    }, 0);
  }
}
