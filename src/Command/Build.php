<?php

namespace Ibuildings\BehatBrowserstack\Command;

use Webmozart\Assert\Assert;

class Build {
  private $seconds;

  /**
   * @var Feature[]
   */
  private $features;

  public function __construct(float $seconds, array $results) {
    $this->seconds = $seconds;
    Assert::allIsInstanceOf($results, Feature::class);
    $this->features = $results;
  }

  public function getBuildTime(): float {
    return $this->seconds;
  }

  /**
   * @return \Ibuildings\BehatBrowserstack\Command\Feature[]
   */
  public function getFeatures(): array {
    return $this->features;
  }

}
