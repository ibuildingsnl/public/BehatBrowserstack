<?php

namespace Ibuildings\BehatBrowserstack\Command;

use Webmozart\Assert\Assert;

class Feature {

  private $name;

  private $duration;

  private $success;

  private $scenarios;

  public static function fromRaw(array $features): array {
    return array_map(function ($raw) {
      return new Feature($raw['title'] ?? '', $raw['duration'], $raw['result'] === 'passed', Scenario::fromRaw($raw['scenarios']));
    }, $features);
  }

  /**
   * BehatFeature constructor.
   *
   * @param string $name
   * @param float $duration
   * @param bool $success
   * @param \Ibuildings\BehatBrowserstack\Command\Scenario[] $scenarios
   */
  public function __construct(string $name, float $duration, bool $success, array $scenarios) {
    Assert::allIsInstanceOf($scenarios, Scenario::class);
    $this->name = $name;
    $this->duration = $duration;
    $this->success = $success;
    $this->scenarios = $scenarios;
  }

  public function getName(): string {
    return $this->name;
  }

  public function getDuration(): float {
    return $this->duration;
  }

  public function isSuccess(): bool {
    return $this->success;
  }

  public function getScenarios(): array {
    return $this->scenarios;
  }

}
