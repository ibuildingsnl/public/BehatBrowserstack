<?php

namespace Ibuildings\BehatBrowserstack\Command;

class Scenario {

  private $name;

  private $passed;

  private $duration;

  public static function fromRaw(array $scenarios) {
    return array_map(function ($raw) {
      return new Scenario($raw['title'], $raw['result'] === 'passed', $raw['duration']);
    }, $scenarios);
  }

  public function __construct(string $name, bool $passed, float $duration) {
    $this->name = $name;
    $this->passed = $passed;
    $this->duration = $duration;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  public function isPassed(): bool {
    return $this->passed;
  }

  /**
   * @return float
   */
  public function getDuration(): float {
    return $this->duration;
  }



}
