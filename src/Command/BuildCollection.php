<?php

namespace Ibuildings\BehatBrowserstack\Command;

class BuildCollection {

  /**
   * @var \Ibuildings\BehatBrowserstack\Command\Build[]
   */
  private $builds = [];

  public function add(Build $stats) {
    $this->builds[] = $stats;
  }

  public function averageBuildTime(): float {
    if ($this->count() == 0) {
      return 0;
    }
    $totalTime = array_reduce($this->builds, function (float $time, Build $stats) {
      return $time + $stats->getBuildTime();
    }, 0.0);
    return $totalTime / $this->count();
  }

  public function count(): int  {
    return count($this->builds);
  }

  public function combinedScenarioStats(): ScenarioStatsCollection {
    $scenarios = new ScenarioStatsCollection();
    foreach ($this->builds as $build) {
      foreach ($build->getFeatures() as $feature) {
        /** @var \Ibuildings\BehatBrowserstack\Command\Scenario $scenario */
        foreach ($feature->getScenarios() as $scenario) {
          $scenarios->get($feature->getName(), $scenario->getName())->add($scenario);
        }
      }
    }
    return $scenarios;
  }
}
