<?php

namespace Ibuildings\BehatBrowserstack\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class ValidateBehatTestQualityCommand extends Command
{
  protected static $defaultName = 'test-quality';

  protected function configure()
  {
    $this
      ->setDescription('Test your behat suite with retries.')

      ->addOption('runs', 'r', InputArgument::OPTIONAL, 'Number of test runs', 1)
      ->addOption('failed-runs', 'f', InputArgument::OPTIONAL, 'Number of extra test runs for failed scenarios', 10)
      ->addOption('timeout', 't', InputArgument::OPTIONAL, 'Timeout in seconds', 60 * 30)
      ->addOption('args', 'a', InputArgument::OPTIONAL, 'Behat arguments', '')
      ->addOption('exit-threshold', 'e', InputArgument::OPTIONAL, 'Exit code 1 % > threshold when a single scenario fails', 30)

      ->setHelp('This command allows you to run test multiple times and create a status rapport')
    ;
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int|void|null
   * @throws \Exception
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $writeMarkerLine = function () use ($output) {
      $output->writeln('------------------------------------');
    };
    $stats = new BuildCollection();

    $runs = $input->getOption('runs');
    $args = $input->getOption('args');
    $timeout = $input->getOption('timeout');
    for ($i = 0; $i < $runs; $i++) {
      $timeLeft = $stats->averageBuildTime();
      $runsLeft = $runs - $i;
      $writeMarkerLine();
      $output->writeln(sprintf('Run %d (Time left %s min)', $i, $timeLeft === 0 ? '...' : round($timeLeft / 60) * $runsLeft, 2), 0);
      $writeMarkerLine();
      $stats->add($this->executeBehatBuild($args, $timeout, $output));
    }
    $collection = $stats->combinedScenarioStats()->orderByFailed();
    $scenarioStats = $collection->getStats();
    $writeMarkerLine();
    foreach($scenarioStats as $scenario) {
      $output->writeln($scenario->toString());
    }
    $writeMarkerLine();
    $output->writeln('Overall:' . $collection->toString());
    $writeMarkerLine();

    if ($collection->getFails() === 0) {
      exit(0);
    }

    $writeMarkerLine();
    $output->writeln('Running failed test now..');

    $failedRuns = $input->getOption('failed-runs');
    $stats = new BuildCollection();
    foreach($scenarioStats as $scenario) {
      if ($scenario->getFails() === 0) {
        continue;
      }
      for ($i = 0; $i < $failedRuns; $i++) {
        $timeLeft = $stats->averageBuildTime();
        $runsLeft = $runs - $i;
        $writeMarkerLine();
        $output->writeln(sprintf('Run %d (Time left %s min) %s', $i, $timeLeft === 0 ? '...' : round($timeLeft / 60) * $runsLeft, $scenario->getLabel()));
        $writeMarkerLine();
        $stats->add($this->executeBehatBuild(sprintf('%s --name \'%s\' --name \'%s\'', $args, $scenario->getFeatureName(), $scenario->getScenarioName()), $timeout, $output));
      }
    }
    $collection = $stats->combinedScenarioStats()
      ->mergeSame($scenarioStats)
      ->orderByFailed();
    $scenarioStats = $collection->getStats();
    $writeMarkerLine();
    $output->writeln('Failed stats:..');
    foreach($scenarioStats as $scenario) {
      $output->writeln($scenario->toString());
    }
    $output->writeln('Failed overall:' . $collection->toString());

    foreach($scenarioStats as $scenario) {
      if (((int) $input->getOption('exit-threshold')) < $scenario->failPercentage()) {
        exit(1);
      }
    }
  }

  private function timeExecution(callable $callback): float {
    $start = microtime(true);
    $callback();
    return microtime(true) - $start;
  }

  private function executeBehatBuild(string $args, int $timeout, OutputInterface $output): Build {
    $process = new Process('vendor/bin/behat --format pretty --format json_formatter ' . $args, NULL, getenv(), null, $timeout);
    // Only keep printing when it's not the json_formatter output.
    $keepPrinting = true;
    $time = $this->timeExecution(function () use ($process, $output, &$keepPrinting) {
      $process->run(function () use ($process, $output, &$keepPrinting) {
        $str = $process->getIncrementalOutput();
        if (!$keepPrinting) {
          if (preg_match('/^\}([\s\S]*?)/m', $str, $matches) === 1) {
            $keepPrinting = true;
            $output->write($matches[1]);
          }
          return;
        }

        if (preg_match('/([\s\S]*?)^\{/m', $str, $matches) === 1) {
          $keepPrinting = false;
          $output->write($matches[1]);
        } else {
          $output->write($str);
        }
      });
    });
    $invalidJsonParts = $process->getOutput();
    preg_match_all('/(^\{[\s\S]*?^\})/m', $invalidJsonParts, $matches, PREG_SET_ORDER, 0);
    $features = [];
    foreach ($matches as $match) {
      $featue = json_decode($match[0], TRUE);
      if (json_last_error() !== JSON_ERROR_NONE) {
        throw new \Exception(sprintf('Failed to execute behat: %s', $process->getErrorOutput()));
      }
      $features = array_merge($features, $featue['features']);
    }
    return new Build($time, Feature::fromRaw($features));
  }
}
