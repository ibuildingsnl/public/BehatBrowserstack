<?php

namespace Ibuildings\BehatBrowserstack\Command;

class ScenarioStats {

  private $passes = 0;

  /**
   * @var float
   */
  private $durations = 0.0;

  private $fails = 0;

  private $featureName;

  private $scenarioName;

  public function __construct(string $featureName, string $scenarioName) {
    $this->featureName = $featureName;
    $this->scenarioName = $scenarioName;
  }

  public function add(Scenario $scenario) {
    if ($scenario->isPassed()) {
      $this->passes += 1;
    } else {
      $this->fails += 1;
    }
    $this->durations += $scenario->getDuration();
  }

  public function addStats(ScenarioStats $stat) {
    $this->passes += $stat->passes;
    $this->fails += $stat->fails;
    $this->durations += $stat->durations;
  }

  public function getPasses(): int {
    return $this->passes;
  }

  public function getAverageDuration(): float {
    $count = $this->count();
    if ($count === 0) {
      return 0;
    }
    return $this->durations / $count;
  }

  public function getFails(): int {
    return $this->fails;
  }

  public function count(): int {
    return $this->passes + $this->fails;
  }

  public function getFeatureName(): string {
    return $this->featureName;
  }

  public function getScenarioName(): string {
    return $this->scenarioName;
  }

  public function getLabel(): string {
    return sprintf('%s (%s)', $this->scenarioName, $this->featureName);
  }

  public function toString(): string {
    $percentage = $this->failPercentage();
    $fail = sprintf('%d/%d', $this->getFails(), $this->count());
    return sprintf('%8s | (%3d%%) | %5.2f | %s', $fail, $percentage, $this->getAverageDuration(), $this->getLabel());
  }

  public function failPercentage(): float  {
    return ((float) $this->getFails()) / ((float) $this->count()) * 100.0;
  }

}
