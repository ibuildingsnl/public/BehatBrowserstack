<?php

namespace Ibuildings\BehatBrowserstack;

use Behat\Testwork\ServiceContainer\Extension;
use Behat\Testwork\ServiceContainer\ExtensionManager;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BrowserstackExtension implements Extension {

  /**
   * You can modify the container here before it is dumped to PHP code.
   */
  public function process(ContainerBuilder $container) {
    // nothing.
  }

  /**
   * Returns the extension config key.
   *
   * @return string
   */
  public function getConfigKey() {
    return 'browserstack';
  }

  public function initialize(ExtensionManager $extensionManager) {
    /** @var \Behat\MinkExtension\ServiceContainer\MinkExtension $minkExtension */
    $minkExtension = $extensionManager->getExtension('mink');
    $minkExtension->registerDriverFactory(new BrowserstackSelenium2Factory());
  }

  public function configure(ArrayNodeDefinition $builder) {
    $builder
      ->children()
        ->scalarNode('debug')->defaultFalse()->end()
      ->end();
  }

  public function load(ContainerBuilder $container, array $config) {
    $container->setParameter('browserstack.debug', $config['debug']);
  }

}
