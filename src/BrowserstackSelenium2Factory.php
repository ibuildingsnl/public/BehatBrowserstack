<?php

namespace Ibuildings\BehatBrowserstack;

use Behat\MinkExtension\ServiceContainer\Driver\BrowserStackFactory;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\Definition;

class BrowserstackSelenium2Factory extends BrowserStackFactory {

  /**
   * {@inheritdoc}
   */
  public function getDriverName() {
    return 'browserstack_selenium_driver';
  }

  /**
   * {@inheritdoc}
   */
  public function buildDriver(array $config) {
    $config['capabilities']['browserstack-tunnel'] = $config['capabilities']['browserstack-tunnel'] ?? true;
    $config['wd_host'] = sprintf('%s:%s@hub.browserstack.com/wd/hub', $config['username'], $config['access_key']);
    $extraCapabilities = $config['capabilities']['extra_capabilities'];
    unset($config['capabilities']['extra_capabilities']);
    $guessedCapabilities = [
      'build' => EnvironmentVariables::build(),
      'name' => 'pending'
    ];
    $client = new Client([
      'headers' => ['Content-Type' => 'application/json'],
      'auth' => [
        EnvironmentVariables::username(),
        EnvironmentVariables::password(),
      ],
    ]);
    return new Definition(BrowserstackSelenium2Driver::class, [
      new BrowserstackClient($client),
      $config['browser'],
      array_replace($extraCapabilities, $config['capabilities'], $guessedCapabilities),
      $config['wd_host'],
      '%browserstack.debug%',
    ]);
  }

}
