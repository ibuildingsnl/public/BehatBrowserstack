<?php

namespace Ibuildings\BehatBrowserstack;

use GuzzleHttp\ClientInterface;

class BrowserstackClient {

  private $client;

  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  public function markFailed(string $sessionId, string $reason): void {
    $this->client->put($this->createSessionEndpointUrl($sessionId), [
      'body' => json_encode([
        'status' => 'failed',
        'reason' => $reason,
      ]),
    ]);
  }

  public function getSession(string $sessionId): \stdClass {
    $result = $this->client->get($this->createSessionEndpointUrl($sessionId));
    return json_decode((string) $result->getBody())->automation_session;
  }

  public function updateName(string $sessionId, string $name): void {
    $this->client->put($this->createSessionEndpointUrl($sessionId), [
      'body' => json_encode([
        'name' => $name,
      ]),
    ]);
  }

  /**
   * @param string $sessionId
   *
   * @return string
   */
  public function createSessionEndpointUrl(string $sessionId): string {
    return sprintf(
      'https://api.browserstack.com/automate/sessions/%s.json',
      $sessionId
    );
  }

}
